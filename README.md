# Vue Library and Jest Unit tests

I've run across an issue trying to run Jest unit tests on a library created with Vue CLI 3.  The issue revolves around running unit tests with Jest in the application consuming the library if that library exports something other than Vue SFC's.

While running the app works, the Jest unit test is unable to correctly import the non-SFC exported items.

## Why Webpack Works

Webpack will use the `module` field in `package.json` to reference the library starting point or the `main` if no `module` exists.

Webpack correctly handles the es6 export statements.

## Why Jest Fails

Jest relies on Babel and not Webpack.  The babel-jest package does not use `module` to identify the library entry point, and only uses `main`.   

https://babeljs.io/blog/2018/06/26/on-consuming-and-publishing-es2015+-packages#conflating-javascript-modules-and-es2015

## Babel fine print

Babel uses the `main` field in the `package.json` file *if* there is no `index.js` file.  Placing the exports in `src/index.js` and setting the `module` field works for Webpack but not Babel.

With the exports in `index.js` and the module set to that, both Webpack and Babel can find all of the exports.


