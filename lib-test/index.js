// eslint-disable no-console
import HelloWorld from "./src/components/HelloWorld.vue";
import { testfunc } from "./src/lib.js";

export { HelloWorld, testfunc };

// export const answer = 42;

// console.log("lib-test:index answer", answer);
console.log("lib-test:index testfunc", testfunc);
console.log("lib-test:index HelloWorld", HelloWorld);
